fullcoverage 💯 Cheat and get 100% test coverage
================================================

    In case this is not clear enough: *this module is a joke !* Do not use it. It is not dangerous; it is just useless.

Nowadays, every trendy project has a collection of shiny little badges. They look great, but they can be hard to get. This module is here to help you to get the 100 % test coverage badge, without any effort.


.. contents::
  :local:
  :backlinks: none

What's new?
-----------

See `changelog <https://git.framasoft.org/spalax/fullcoverage/blob/main/CHANGELOG.md>`_.

Download and install
--------------------

* From sources:

  * Download: https://pypi.python.org/pypi/fullcoverage
  * Install (in a `virtualenv`, if you do not want to mess with your distribution installation system)::

        python3 setup.py install

* From pip::

    pip install fullcoverage

* Quick and dirty Debian (and Ubuntu?) package

  This requires `stdeb <https://github.com/astraw/stdeb>`_ to be installed::

      python3 setup.py --command-packages=stdeb.command bdist_deb
      sudo dpkg -i deb_dist/fullcoverage-<VERSION>_all.deb

How-to
------

Let's say you want to get you module ``foobar`` thoroughly covered.

#. Install `fullcoverage`::

    pip install fullcoverage

#. Enable this plugin. That is, in the `.coveragerc` file of ``foobar``, have the lines::

    [run]
    plugins = fullcoverage.plugin

#. Optionnaly, if you want to restrict the test report to your module, you can add, in the same `.coveragerc` file::

    [fullcoverage.plugin]
    source = foobar

#. If you already have tests that import all of ``foobar`` submodules, that's it. Otherwise, somewhere in your tests, have the following lines import everything in your module:

    .. code-block:: python

       import foobar
       import fullcoverage

       fullcoverage.import_all(foobar)

#. That's it! You can now run your tests::

    coverage run --source foobar -m unittest
    coverage report --fail-under=100

Bugs and Limitations
--------------------

* This does not work with ``doctest``. Please feel free to `send a patch <http://git.framasoft.org/spalax/fullcoverage/issues/new>`__.
* Even though tests cover 100 % of your module, it may still contain bugs.

FAQ
---

* Isn't it dangerous? Are you running every line of a module to get 100 % test coverage? What if you run a line ``shutil.rmtree('/')`` ?

This plugins does not run random line of codes. It imports every plugin that is to be covered, but it cheats ``coverage`` into thinking they were thoroughly tested, without testing anything. If your module is safe to import, it is safe to use ``fullcoverage``. If your module is not safe to import, fix your module.
