* fullcoverage 0.1.1 (unreleased)

    * Drop python3.5 support.
    * Add python3.6 to python3.10 support

    -- Louis Paternault <spalax@gresille.org>

* fullcoverage 0.1.0 (2016/03/24)

    * First release

    -- Louis Paternault <spalax@gresille.org>
